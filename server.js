const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const lowdb  = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');
const adapter = new FileSync('lowdb.json');
const db = lowdb(adapter);

app.set('view engine', 'pug');
app.set('views','./views');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));


app.get('/', (request, response) => {
  response.send('<a href="./todos">Link</a>')
});

app.get('/todos', (req, res) => {
  if (req.query.q) {
    let keyword = req.query.q;
    let queryAll = db.get('todos').value();
    let result = queryAll.filter((item)=> {
      return item.content.toLowerCase().indexOf(keyword.toLowerCase()) >=0;
    });
    res.render('index',{todoList: result, keyword: keyword});
  }
  else {
    let queryAll = db.get('todos').value();
    res.render('index',{todoList: queryAll});
  }
});


app.post('/todos/create', (req,res)=> {
  let data = req.body;
  db.get('todos')
    .push(data)
    .write();

  res.redirect("back");
})
// listen for requests :)
app.listen(3000, () => {
  console.log('Server listening on port ' + process.env.PORT);
});
